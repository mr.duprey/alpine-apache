####################################################
# Image Docker: Alpine 3.8
# Add-ons: OpenRC & Apche2
# Author: Raymar Duprey | r.duprey@rayops.com
# ++++++++++++++++++++++++++++++++++++++++++++++++++
# Build: $ docker build -t alpine-apache .
# Run:   $ docker run -p 8080:80 alpine-apache
####################################################
# Reference:
# - https://github.com/gliderlabs/docker-alpine/
# - https://github.com/neeravkumar/dockerfiles
###################################################
FROM scratch

ADD ./app/rootfs.tar.xz /

RUN mkdir -p /etc/apk && echo "http://alpine.gliderlabs.com/alpine/v3.8/main/" > /etc/apk/repositories &&\
# Install openrc
    apk update && apk --no-cache add openrc apache2 &&\
# Start services when boot
	rc-update add apache2 default && \
# can't get ttys unless you run the container in privileged mode
    sed -i '/tty/d' /etc/inittab && \
# can't set hostname since docker sets it
    sed -i 's/hostname $opts/# hostname $opts/g' /etc/init.d/hostname &&\
# can't mount tmpfs since not privileged
    sed -i 's/mount -t tmpfs/# mount -t tmpfs/g' /lib/rc/sh/init.sh

COPY ./app/rc.conf /etc

COPY ./app/httpd.conf /etc/apache2/

COPY ./app/rc-cgroup.sh /lib/rc/sh/

COPY ./public-html/ /var/www/localhost/htdocs

EXPOSE 80 443

CMD ["/sbin/init"]
